using System;

namespace MFA.BI.Web.Models
{
    public class MediaStatInfo
    {
        public MediaStat media_stat { get; set; }
    }

    public class MediaStat
    {
        public AxisData[] video { get; set; }

        public AxisData[] audio { get; set; }

        public AxisData[] photo_album { get; set; }

        public AxisData[] comment { get; set; }

        public AxisData[] charge { get; set; }

        public AxisData[] consume { get; set; }

        public AxisData[] profit { get; set; }
    }

}
