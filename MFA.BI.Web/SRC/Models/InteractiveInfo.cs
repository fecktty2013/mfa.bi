using System;

namespace MFA.BI.Web.Models
{
    public class InteractiveInfo
    {
        public UVDetail uv_detail { get; set; }
    }

    public class UVDetail
    {
        public AxisData[] register { get; set; }

        public AxisData[] active { get; set; }

        public AxisData[] thump_up { get; set; }

        public AxisData[] comment { get; set; }

        public AxisData[] charge { get; set; }

        public AxisData[] consume { get; set; }

        public AxisData[] profit { get; set; }
    }

    public class AxisData {
        public string dt { get; set; }
        public int count { get; set; }
    }

}
