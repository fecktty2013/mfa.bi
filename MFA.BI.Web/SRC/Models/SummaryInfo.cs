using System;

namespace MFA.BI.Web.Models
{
    public class SummaryInfo
    {
        public UVStat uv_stat { get; set; }

        public EntityStat entity_stat { get; set; }

        public MemberStat member_stat { get; set; }

        public NoticeStat notice_stat { get; set; }

    }

    public class UVStat
    {
        public int register_num { get; set; }

        public int active_num { get; set; }

        public int thumb_up_num { get; set; }

        public int comment_num { get; set; }

        public int charge_num { get; set; }

        public int consume_num { get; set; }

        public int profit_num { get; set; }
    }

    public class EntityStat
    {
        public int video_normal { get; set; }

        public int video_frozen { get; set; }

        public int audio_normal { get; set; }

        public int audio_frozen { get; set; }

        public int photo_album_normal { get; set; }

        public int photo_album_frozen { get; set; }

        public int comment_total { get; set; }

        public int comment_verified { get; set; }
    }

    public class MemberStat
    {
        public int member_total { get; set; }

        public int member_frozen { get; set; }
    }

    public class NoticeStat
    {
        public int new_feedback { get; set; }

        public int new_comment { get; set; }
    }
}
