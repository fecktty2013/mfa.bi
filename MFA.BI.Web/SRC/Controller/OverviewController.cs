using System;
using System.Linq;
using MFA.BI.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MFA.BI.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OverviewController : ControllerBase
    {
        private readonly ILogger<OverviewController> _logger;

        public OverviewController(ILogger<OverviewController> logger)
        {
            _logger = logger;   
        }

        // GET: overview/summaryInfo
        [HttpGet("summaryInfo")]
        public SummaryInfo SummaryInfo() {
            return new SummaryInfo() {
                uv_stat = new UVStat() {
                    register_num = 43,
                    active_num = 5,
                    thumb_up_num = 24,
                    comment_num = 2,
                    charge_num = 1,
                    consume_num = 80,
                    profit_num = 8
                },
                entity_stat = new EntityStat() {
                    video_normal = 16,
                    video_frozen = 2,
                    audio_normal = 8,
                    audio_frozen = 0,
                    photo_album_normal = 12,
                    photo_album_frozen = 0,
                    comment_total = 2,
                    comment_verified = 2
                },
                member_stat = new MemberStat() {
                    member_total = 40,
                    member_frozen = 3
                },
                notice_stat = new NoticeStat() {
                    new_feedback = 2,
                    new_comment = 1
                }
            };
        }

        // GET: overview/interactiveInfo
        [HttpGet("interactiveInfo")]
        public InteractiveInfo InteractiveInfo() {
            return new InteractiveInfo() {
                uv_detail = new UVDetail() {
                    register = new AxisData[] {
                        new AxisData() { dt = "2020-03-14T21:59:37.776Z", count = 8 },
                        new AxisData() { dt = "2020-03-16T14:09:37.776Z", count = 15 },
                        new AxisData() { dt = "2020-03-18T10:13:37.776Z", count = 23 },
                        new AxisData() { dt = "2020-03-20T11:29:37.776Z", count = 2 },
                        new AxisData() { dt = "2020-03-24T13:24:37.776Z", count = 6 },
                    },
                    active = new AxisData[] {
                        new AxisData() { dt = "2020-03-14T21:59:37.776Z", count = 6 },
                        new AxisData() { dt = "2020-03-15T14:29:37.776Z", count = 18 },
                        new AxisData() { dt = "2020-03-17T18:30:37.776Z", count = 24 },
                        new AxisData() { dt = "2020-03-19T22:20:37.776Z", count = 13 },
                        new AxisData() { dt = "2020-03-23T14:15:37.776Z", count = 20 },
                    },
                    thump_up = new AxisData[] {
                        new AxisData() { dt = "2020-03-14T21:59:37.776Z", count = 3 },
                        new AxisData() { dt = "2020-03-15T14:29:37.776Z", count = 5 },
                        new AxisData() { dt = "2020-03-17T18:30:37.776Z", count = 2 },
                        new AxisData() { dt = "2020-03-19T22:20:37.776Z", count = 8 },
                        new AxisData() { dt = "2020-03-23T14:15:37.776Z", count = 6 },
                    },
                    comment = new AxisData[] {
                        new AxisData() { dt = "2020-03-14T21:59:37.776Z", count = 3 },
                        new AxisData() { dt = "2020-03-15T14:29:37.776Z", count = 5 },
                        new AxisData() { dt = "2020-03-17T18:30:37.776Z", count = 2 },
                        new AxisData() { dt = "2020-03-19T22:20:37.776Z", count = 8 },
                        new AxisData() { dt = "2020-03-23T14:15:37.776Z", count = 6 },
                    },
                    charge = new AxisData[] {
                        new AxisData() { dt = "2020-03-14T07:59:37.776Z", count = 13 },
                        new AxisData() { dt = "2020-03-15T12:29:37.776Z", count = 15 },
                        new AxisData() { dt = "2020-03-17T19:30:37.776Z", count = 12 },
                        new AxisData() { dt = "2020-03-19T23:20:37.776Z", count = 18 },
                        new AxisData() { dt = "2020-03-23T15:15:37.776Z", count = 16 },
                    },
                    consume = new AxisData[] {
                        new AxisData() { dt = "2020-03-15T07:59:37.776Z", count = 3 },
                        new AxisData() { dt = "2020-03-15T15:29:37.776Z", count = 8 },
                        new AxisData() { dt = "2020-03-17T07:30:37.776Z", count = 4 },
                        new AxisData() { dt = "2020-03-19T13:20:37.776Z", count = 9 },
                        new AxisData() { dt = "2020-03-22T11:15:37.776Z", count = 5 },
                    },
                    profit = new AxisData[] {
                        new AxisData() { dt = "2020-03-15T07:59:37.776Z", count = 1 },
                        new AxisData() { dt = "2020-03-15T15:29:37.776Z", count = 2 },
                        new AxisData() { dt = "2020-03-17T07:30:37.776Z", count = 2 },
                        new AxisData() { dt = "2020-03-19T13:20:37.776Z", count = 5 },
                        new AxisData() { dt = "2020-03-22T11:15:37.776Z", count = 3 },
                    }
                }
            };
        }

        // GET: overview/mediaStatInfo
        [HttpGet("mediaStatInfo")]
        public MediaStatInfo MediaStatInfo() {
            return new MediaStatInfo() {
                media_stat = new MediaStat() {
                    video = new AxisData[] {
                        new AxisData() { dt = "2020-03-15T07:59:37.776Z", count = 3 },
                        new AxisData() { dt = "2020-03-16T15:29:37.776Z", count = 2 },
                        new AxisData() { dt = "2020-03-17T08:30:37.776Z", count = 4 },
                        new AxisData() { dt = "2020-03-20T13:21:37.776Z", count = 5 },
                        new AxisData() { dt = "2020-03-22T11:15:37.776Z", count = 1 },
                    },
                    audio = new AxisData[] {
                        new AxisData() { dt = "2020-03-15T17:59:37.776Z", count = 1 },
                        new AxisData() { dt = "2020-03-16T05:29:37.776Z", count = 5 },
                        new AxisData() { dt = "2020-03-17T09:30:37.776Z", count = 3 },
                        new AxisData() { dt = "2020-03-20T10:21:37.776Z", count = 2 },
                        new AxisData() { dt = "2020-03-22T12:15:37.776Z", count = 3 },
                    },
                    photo_album = new AxisData[] {
                        new AxisData() { dt = "2020-03-15T11:59:37.776Z", count = 2 },
                        new AxisData() { dt = "2020-03-16T15:29:37.776Z", count = 3 },
                        new AxisData() { dt = "2020-03-17T11:30:37.776Z", count = 5 },
                        new AxisData() { dt = "2020-03-20T12:21:37.776Z", count = 8 },
                        new AxisData() { dt = "2020-03-22T13:15:37.776Z", count = 3 },
                    },
                }
            };
        }
    }
}