import axios from "axios";

const BASE_URL = '/bi/api';

const httpInstance = axios.create({
    baseURL: BASE_URL,
    timeout: 30000,
});

httpInstance.defaults.headers.post['Content-Type'] = 'application/json';

export default httpInstance;
