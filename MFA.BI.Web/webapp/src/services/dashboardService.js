import http from './http';

export function fetchSummaryInfo() {
    return http.get("/overview/summaryInfo");
}

export function fetchInteractiveInfo(payload) {
    return http.get("/overview/interactiveInfo", {
        params: payload
    });
}

export function fetchMediaStat(payload) {
    return http.get("/overview/mediaStatInfo", {
        params: payload
    });
}