import React, { useEffect, useState } from "react";
import classNames from "classnames";
import moment from "moment";
import { Breadcrumb, Icon, Button, Row, Col, Radio, DatePicker, Tabs, List, Card } from "antd";
import echarts from "echarts";
import * as service from "../services/dashboardService";

import style from "./Dashboard.module.scss";

export const Dashboard = (props) => {
    const chartDiv = React.useRef(null);
    const videoChart = React.useRef(null);
    const photoChart = React.useRef(null);
    const audioChart = React.useRef(null);

    const [statType, setStatType] = useState("day");
    const [statDateRange, setStatDateRange] = useState();

    const [mediaStatType, setMediaStatType] = useState("day");
    const [mediaStatDateRange, setMediaStatDateRange] = useState();

    const [statSummaryInfo, setStatSummaryInfo] = useState({});
    const [entityStat, setEntityStat] = useState({});
    const [memberState, setMemberState] = useState({});
    const [noticeState, setNoticeState] = useState({});

    const queryStat = async () => {
        const summary = await service.fetchSummaryInfo();
        setStatSummaryInfo(summary.data.uv_stat || {});
        setEntityStat(summary.data.entity_stat || {});
        setMemberState(summary.data.member_stat || {});
        setNoticeState(summary.data.notice_stat || {});
    };

    const queryInteractive = async () => {
        const interactiveInfo = await service.fetchInteractiveInfo({
            dateRange: statDateRange || [],
            statType,
        });
        drawStatChart(interactiveInfo.data.uv_detail);
    };

    const queryMediaStat = async () => {
        const result = await service.fetchMediaStat({
            dateRange: mediaStatDateRange || [],
            statType: mediaStatType
        });
        drawVideoChart(result.data.media_stat);
        drawAudioChart(result.data.media_stat);
        // drawPhotoChart(result.media_stat);
    };

    const getStatOption = (data) => {
        const options = {
            title: {
                text: 'Statistics',
                textStyle: {
                    fontSize: 14,
                    fontWeight: 500
                },
                padding: [10, 20]
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                right: 40,
                top: 28,
                data: ['Active', 'Comments', 'Registered', 'Likes', 'Recharge', 'Consumption', 'Earnings']
            },
            grid: {
                left: '40',
                right: '20',
                bottom: '40',
                containLabel: true
            },
            xAxis: {
                type: 'time',
                boundaryGap: false,
                splitLine: {
                    show: false,
                    interval: 'auto'
                },
                axisLine: {
                    lineStyle: {
                        color: '#CCC',
                    }
                },
                axisLabel: {
                    margin: 12,
                    color: '#333'
                }
            },
            yAxis: {
                type: 'value',
                offset: 20,
                nameTextStyle: {
                    align: 'left'
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        type: 'dashed'
                    }
                },
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                }
            },
            dataZoom: [{
                type: 'inside',
                start: 0,
                end: 100,
            }, {
                start: 0,
                end: 100,
                handleIcon: 'M-292,322.2c-3.2,0-6.4-0.6-9.3-1.9c-2.9-1.2-5.4-2.9-7.6-5.1s-3.9-4.8-5.1-7.6c-1.3-3-1.9-6.1-1.9-9.3c0-3.2,0.6-6.4,1.9-9.3c1.2-2.9,2.9-5.4,5.1-7.6s4.8-3.9,7.6-5.1c3-1.3,6.1-1.9,9.3-1.9c3.2,0,6.4,0.6,9.3,1.9c2.9,1.2,5.4,2.9,7.6,5.1s3.9,4.8,5.1,7.6c1.3,3,1.9,6.1,1.9,9.3c0,3.2-0.6,6.4-1.9,9.3c-1.2,2.9-2.9,5.4-5.1,7.6s-4.8,3.9-7.6,5.1C-285.6,321.5-288.8,322.2-292,322.2z',
                handleSize: '80%',
                handleStyle: {
                    color: '#fff',
                    shadowBlur: 3,
                    shadowColor: 'rgba(0, 0, 0, 0.6)',
                    shadowOffsetX: 2,
                    shadowOffsetY: 2
                }
            }],
            series: [
                {
                    name: 'Active Users',
                    type: 'line',
                    data: (data?.active || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
                },
                {
                    name: 'Comments',
                    type: 'line',
                    data: (data?.comment || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
                },
                {
                    name: 'Registered',
                    type: 'line',
                    data: (data?.register || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
                },
                {
                    name: 'Likes',
                    type: 'line',
                    data: (data?.thumb_up || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
                },
                {
                    name: 'Recharge',
                    type: 'line',
                    data: (data?.charge || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
                },
                {
                    name: 'Consumption',
                    type: 'line',
                    data: (data?.consume || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
                },
                {
                    name: 'Earnings',
                    type: 'line',
                    data: (data?.profit || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
                },
            ]
        };
        return options;
    };

    const getMediaChartOption = (name, data) => {
        const options = {
            color: ['#1890ff'],
            title: {
                text: 'Count',
                textStyle: {
                    fontSize: 14,
                    fontWeight: 500,
                },
                padding: [10, 20],
            },
            tooltip: {
                trigger: 'axis',
            },
            grid: {
                left: '40',
                right: '20',
                bottom: '40',
                containLabel: true
            },
            xAxis: {
                type: 'time',
                // boundaryGap: true,
                // data: ['10月', '11月', '12月', '1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月'],
                splitLine: {
                    show: false,
                    interval: 'auto',
                },
                axisLine: {
                    lineStyle: {
                        color: '#ccc',
                    },
                },
                axisTick: {
                    alignWithLabel: true
                },
                axisLabel: {
                    margin: 12,
                    color: '#333',
                },
            },
            yAxis: {
                type: 'value',
                offset: 20,
                nameTextStyle: {
                    align: 'left',
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        type: 'dashed',
                    }
                },
                minInterval: 1,
                axisLine: {
                    show: false,
                },
                axisTick: {
                    show: false,
                },
            },
            series: [
                {
                    name: name,
                    type: 'bar',
                    barWidth: '40%',
                    data: (data || []).map(item => ([moment(item.dt).toDate(), item.count.toString()])),
                }
            ],
        };
        return options;
    };

    const drawStatChart = (data) => {
        console.log('chart data', data);
        let options = getStatOption(data);
        if (chartDiv.current) {
            let lineChart = echarts.init(chartDiv.current);
            lineChart.setOption(options);
        }
    };

    const drawVideoChart = (data) => {
        let option = getMediaChartOption('Video Count', data?.video);
        let videoBarChart = echarts.init(videoChart.current);
        videoBarChart.setOption(option);
    };

    const drawAudioChart = (data) => {
        let option = getMediaChartOption('Audio Count', data?.audio);
        let audioBarChart = echarts.init(audioChart.current);
        audioBarChart.setOption(option);
    };

    const drawPhotoChart = (data) => {
        let option = getMediaChartOption('Photo Count', data?.photo_album);
        let photoBarChart = echarts.init(photoChart.current);
        photoBarChart.setOption(option);
    };

    const handleStatTypeChanged = (e) => {
        setStatType(e.target.value);
    };

    const handleStatDateRangeChanged = (date, dateString) => {
        setStatDateRange(date);
    };

    const handleMediaStatTypeChanged = (e) => {
        setMediaStatType(e.target.value);
    };

    const handleMediaStatDateRangeChanged = (date, dateString) => {
        setMediaStatDateRange(date);
    };

    useEffect(queryStat, []);

    useEffect(queryInteractive, [statType, statDateRange]);

    useEffect(queryMediaStat, [mediaStatType, mediaStatDateRange]);

    let contentSummary = [
        { category: 'Regular Video', total: entityStat.video_album_normal || 0 },
        { category: 'Frozen Video', total: entityStat.video_album_frozen || 0 },
        { category: 'Regular Photo', total: entityStat.photo_album_normal || 0 },
        { category: 'Frozen Photo', total: entityStat.photo_album_frozen || 0 },
        { category: 'Regular Audio', total: entityStat.audio_album_normal || 0 },
        { category: 'Frozen Audio', total: entityStat.audio_album_frozen || 0 },
        { category: 'Comments', total: (entityStat.comment_verified || 0) + '/' + (entityStat.comment_total || 0) },
    ];

    let memberSummary = [
        { category: 'Regular Member', total: memberState.member_total || 0 },
        { category: 'Suspended Member', total: memberState.member_frozen || 0 },
    ];

    return (
        <div className={style.container}>
            <div className="breadCrumbContainer">
                <Breadcrumb>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                    <Breadcrumb.Item>Overview</Breadcrumb.Item>
                </Breadcrumb>
            </div>
            <div className="pageBody">
                <div className={classNames('appRow', style.topRow)}>
                    <div className={classNames('appCol', style.leftNav)}>
                        <Icon style={{ fontSize: 10 }} type="left" />
                    </div>
                    <div className={classNames('appCol', style.chartCategory)}>
                        <span>Registered</span>
                        <span>{statSummaryInfo.register_num || 0}</span>
                        <span>Users</span>
                    </div>
                    <div className={classNames('appCol', style.chartCategory)}>
                        <span>Active</span>
                        <span>{statSummaryInfo.active_num || 0}</span>
                        <span>Users</span>
                    </div>
                    <div className={classNames('appCol', style.chartCategory)}>
                        <span>Likes</span>
                        <span>{statSummaryInfo.thumb_up_num || 0}</span>
                        <span>Times</span>
                    </div>
                    <div className={classNames('appCol', style.chartCategory)}>
                        <span>Comments</span>
                        <span>{statSummaryInfo.comment_num || 0}</span>
                        <span>Times</span>
                    </div>
                    <div className={classNames('appCol', style.chartCategory)}>
                        <span>Recharge</span>
                        <span>{statSummaryInfo.charge_num || 0}</span>
                        <span>CAD</span>
                    </div>
                    <div className={classNames('appCol', style.chartCategory)}>
                        <span>Consumption</span>
                        <span>{statSummaryInfo.consume_num || 0}</span>
                        <span>CAD</span>
                    </div>
                    <div className={classNames('appCol', style.chartCategory)}>
                        <span>Earnings</span>
                        <span>{statSummaryInfo.profit_num || 0}</span>
                        <span>CAD</span>
                    </div>
                    <div className={classNames('appCol', style.rightNav)}>
                        <Icon style={{ fontSize: 10 }} type="right" />
                    </div>
                </div>
                <div className={style.statPanel}>
                    <Row gutter={8} type="flex" className={style.statAction}>
                        <Col>
                            <Radio.Group onChange={handleStatTypeChanged} value={statType}>
                                <Radio.Button value="day">Today</Radio.Button>
                                <Radio.Button value="week">Week</Radio.Button>
                                <Radio.Button value="month">Month</Radio.Button>
                                <Radio.Button value="year">Year</Radio.Button>
                                <Radio.Button value="all">All</Radio.Button>
                            </Radio.Group>
                        </Col>
                        <Col>
                            <DatePicker.RangePicker onChange={handleStatDateRangeChanged} value={statDateRange} style={{ width: 240 }} />
                        </Col>
                        <Col>
                            <Button>Export Report</Button>
                        </Col>
                    </Row>
                    <div className={style.statChartContainer} ref={chartDiv} />
                </div>
                <div className={style.contentPanel}>
                    <Row gutter={16} style={{ height: '100%' }}>
                        <Col span={18}>
                            <Tabs size="large" tabBarGutter={16}
                                tabBarExtraContent={
                                    <React.Fragment>
                                        <Radio.Group onChange={handleMediaStatTypeChanged} value={mediaStatType}>
                                            <Radio.Button value="day">Today</Radio.Button>
                                            <Radio.Button value="week">Week</Radio.Button>
                                            <Radio.Button value="month">Month</Radio.Button>
                                            <Radio.Button value="year">Year</Radio.Button>
                                            <Radio.Button value="all">All</Radio.Button>
                                        </Radio.Group>
                                        <DatePicker.RangePicker onChange={handleMediaStatDateRangeChanged} value={mediaStatDateRange} style={{ width: 240 }} />
                                    </React.Fragment>
                                }>
                                <Tabs.TabPane forceRender tab="Video" key="video">
                                    <div ref={videoChart} className={style.barChartContainer} />
                                </Tabs.TabPane>
                                {/* <Tabs.TabPane forceRender tab="Photo" key="album">
                                    <div ref={photoChart} className={style.barChartContainer} />
                                </Tabs.TabPane> */}
                                <Tabs.TabPane forceRender tab="Audio" key="audio">
                                    <div ref={audioChart} className={style.barChartContainer} />
                                </Tabs.TabPane>
                            </Tabs>
                        </Col>
                        <Col span={6}>
                            <List size="small"
                                className={style.summary}
                                header={<div className={style.summaryHeader}>Summary</div>}
                                bordered={false}
                                split={false}
                                dataSource={contentSummary}
                                renderItem={(item, idx) => (
                                    <List.Item className={style.listItem}>
                                        <span className={idx <= 2 ? style.starred : ''}>{idx + 1}</span>
                                        <span>{item.category}</span>
                                        <span>{item.total}</span>
                                    </List.Item>
                                )}
                            />
                        </Col>
                    </Row>
                </div>
                <div className={style.notificationPanel}>
                    <Card title="Notifications" size="default" bordered={false}>
                        <p>You have <span className={style.notifyCount}>{noticeState.new_feedback}</span> feedbacks to check out</p>
                        <p>You have <span className={style.notifyCount}>{noticeState.new_comment}</span> comments to check and review</p>
                    </Card>
                </div>
            </div>
        </div>
    );
}