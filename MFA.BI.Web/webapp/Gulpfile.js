const gulp = require('gulp');

function build() {
    return gulp.src("build/static/**")
      .pipe(gulp.dest('build/bi/static/'));
}

exports.build = build;
